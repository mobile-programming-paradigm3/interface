import 'package:ex1/ex1.dart' as ex1;

void main(List<String> arguments) {
  // Interface s1 = Interface();
  // s1.display();
  var all1 = Alligator();
  all1.hunt();
}

class Interface {
  void display() {
    print(" This is the function of the interface class. ");
  }
}

class Subclass implements Interface {
  @override
  void display() {
    print(" This is the function of the subclass. ");
  }
}

abstract class Reptile with Swim, Bite, Crawl {
  void hunt() {
    print('Alligator -------');
    swim();
    crawl();
    bite();
    print('Eat Fish');
  }
}

class Alligator extends Reptile {}

class Crocodile extends Reptile {}

class fish with Swim, Bite {}

mixin Swim {
  void swim() => print('Swimming');
}

mixin Bite {
  void bite() => print('Chomp');
}

mixin Crawl {
  void crawl() => print('Crawling');
}
